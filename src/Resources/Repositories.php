<?php

namespace Bit\Resources;

use GuzzleHttp\Client;

class Repositories
{
    public function __construct()
    {
        $this->client = new Client([
            // build url.. have 1 client for bitbucket
            // build up url with resources
            'base_uri' => 'https://api.bitbucket.org/2.0/repositories/',
            // authentication
            'auth' => ['jjgrainger', 'Tarquin21']
        ]);
    }

    public function client()
    {
        return $this->client;
    }

    // pass the team or user the repository belongs tp
    public function username($username)
    {
        $this->username = $username;

        return $this;
    }

    // pass the repository slug
    public function repo($repository)
    {
        $this->repo = $repository;

        return $this;
    }

    // perform get request on the URL
    public function get()
    {

        // var_dump($this->client);
        // exit;
        $url = $this->username . '/' . $this->repo;
        $response = $this->client()->get($url);
        var_dump((string) $response->getBody());
    }

}
