<?php

/*

    use Bit/Manager as Bitbucket;

    Bitbucket::

    Bitbucket::api('repositories')->username('jjgrainger')->repository('bitbucket')->get();

    $pipeline = Bitbucket::api('respositories');

    $bit->repositories()->username('jjgrainger')->repo('')



 */

namespace Bit;

class Manager
{

    public static function api($resource)
    {

        $class = "Bit\\Resources\\" . ucfirst($resource);

        return new $class;
    }
}
