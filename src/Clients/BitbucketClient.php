<?php


class BitbucketClient
{

    var $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.bitbucket.org/2.0/',
        ]);
    }
}
